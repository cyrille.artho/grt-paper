\section{Background and Related Work}
\label{sec:background}

The quality of a test suite is often measured by code \emph{coverage},
%including \emph{instruction coverage} to measure whether individual
%instructions have been executed,
such as \emph{branch coverage} to measure
if both branches of a conditional statement have been taken~\cite{Miller1963,Ammann:2008:IST:1355340},
and by \emph{mutation\ score}~\cite{Jia:2011:ASD:2052439.2052494} to assess to what extent the faults seeded to the software under test can be detected by the given tests.
% is another common measurement for the quality of a test suite~\cite{Jia:2011:ASD:2052439.2052494}.
%A set of faulty programs called \emph{mutants} are executed against 
%the given test suite, and mutation score assesses to what extent the seeded faults can be detected.


\subsection{Feedback-directed Random Testing}

Random testing~\cite{Ham94} 
feeds the program under test with randomly generated inputs.
It is easy to use, straightforward to automate, and scalable. 
Random testing has been found effective in detecting
program errors~\cite{Forrester:2000:ESR:1267102.1267108,SPE:SPE602,Oriat:2005:JTR:2101069.2101097,Pacheco:2007:RFR:1297846.1297902}.

\begin{figure}
\centering
\includegraphics[scale=.78]{workflow/Defaultrandoop}
\caption{Workflow of feedback-directed random testing.}\label{fig:default-randoop}
\end{figure}

As a more sophisticated variant of traditional random testing, 
Feedback-directed Random Testing~\cite{Pacheco:2005:EAG:2144892.2144921,Pacheco:2007:RFR:1297846.1297902,Zheng:2010:RUG:1858996.1859054,Jaygarl:2010:OOC:1831708.1831729,Zhang:2011:CSD:2001420.2001463}, uses feedbacks
obtained from test execution to further improve test generation.

Fig.~\ref{fig:default-randoop} shows the essential workflow of FRT techniques.
Initially, all publicly visible API methods of the software under test are put into a fixed \emph{method pool};
simple primitive values and a few simple objects such as strings are added to an initial \emph{object pool}.
Then, the core iteration of test generation starts.
In each iteration, an MUT $m(T_1\ldots T_k)$ is randomly selected from its method pool for testing.
All input objects with type $T_1\ldots T_k$ must also be prepared to test $m$.
To derive these objects, the corresponding method sequences are selected from the object pool as the inputs 
and sometimes concatenated as new sequences.
%Randoop randomly selects the corresponding inputs from its object pool
%and concatenates previously known input sequences to derive these known
%objects.
If all inputs are available, $m$ is executed; methods with incomplete inputs are skipped.
Execution results of each method call are
analyzed against a few predefined contracts.
If a generated sequence is new and its execution does not cause any failures, 
the sequence is marked as a \emph{successful sequence} and added to the object pool as input candidates
for further test iteration~(see Fig.~\ref{fig:default-randoop}).
%\footnote{It is assumed that 
%execution sequences are deterministic.}
The iteration continues to test more methods until a time limit.


\subsection{Issues with Feedback-directed Random Testing}
We identify several common concerns that potentially influence the effectiveness of FRT:
%\begin{enumerate}

\textbf{Selection of initial values.} Although random testing generally produces test inputs randomly,
 	to compose useful complex objects, suitable initial values, 
	 especially primitive values,
	  are required as the basis of input generation. 

\textbf{Selection of MUT.} To achieve higher
coverage, non-covered MUTs are natural targets for generating new test cases.
Run-time coverage information can be used to steer the process
of test generation towards MUTs whose code is not well covered.

 \textbf{Selection of methods.}
Object-oriented testing executes methods and usually requires objects as input arguments.
The selection of the right methods to generate valid sequences~(that return objects)
is a central and difficult problem in random testing. We identify two main aspects of method selection:
	\begin{itemize}

	        \item  \textbf{Properties of methods.} The main properties of a method are characterized
		by the method signature, including the return type and types of parameters.
		In object-oriented programs, high-level supertypes, instead of exact subtypes, are often 
		used to declare parameters for the purpose of design flexibility (i.\,e., to allow run-time 
		substitution of objects with compatible types). Considering both static and dynamic 
		type information can often capture properties of methods more accurately.
		Moreover, the \emph{purity} of methods~\cite{Salcianu:2005:PSE:2131753.2131773},
		whether they have side effects or not, 
		is an important method property relevant to test generation.
	
%	\textbf{Dependencies of methods.}
                \item \textbf{Dependencies of methods.}
%		It is usually infeasible to try all applicable methods~(MUTs and their dependency methods)
%		when trying to generate an object as the input.
		It is usually infeasible to put all applicable methods~(MUTs and their dependent methods)
		into the method pool, because it wastes much testing effort on methods
		that are not the targets.
		However, if a technique uses a limited set of methods (e.\,g., only the MUTs), some objects
		of specific types are unable to be created by FRT, making all the
                 MUTs that depend on such objects untested.
%		the resultant
%                 method sequences are less likely to create some desirable objects of specific types, causing some
%                 MUTs depend such object as input unable to be tested.
                 Thus, the scope of methods need to be chosen appropriately.
		The dependencies of methods can be explored to identify relevant methods and classes.
	\end{itemize}

%%	\begin{itemize}
%
%	         {\textit{1) Properties of methods:}} The main properties of a method are characterized
%		by the method signature, including the return type and types of parameters.
%		In object-oriented programs, high-level supertypes, instead of exact subtypes, are often 
%		used to declare parameter for the purpose of design flexibility (i.\,e., to allow run-time 
%		substitution of objects with compatible types). Considering both static and dynamic 
%		type information can often capture properties of methods more accurately.
%		Moreover, the \emph{purity} of methods~\cite{Salcianu:2005:PSE:2131753.2131773},
%		whether they have the side effect or not, 
%		is an important method property relevant to test generation.
%	
%%	\textbf{Dependencies of methods.}
%		{\textit{2) Dependencies of methods:}}	        
%%		It is usually infeasible to try all applicable methods~(MUTs and their dependency methods)
%%		when trying to generate an object as the input.
%		It is usually infeasible to put all applicable methods~(MUTs and their dependency methods)
%		into the method pool for testing because this wastes much testing effort on methods
%		that are not the targets~(MUTs).
%		However, if a technique uses a limited set of methods, like only the MUTs, some objects
%		of specific types are unable to be created during feedback-directed random testing, causing those
%                 MUTs that depend on such objects as inputs untested.
%%		the resultant
%%                 method sequences are less likely to create some desirable objects of specific types, causing some
%%                 MUTs depend such object as input unable to be tested.
%                 Thus, the scope of methods must be chosen appropriately.
%                 As in object-oriented testing, arguments of MUTs are created
%		by sequences of method calls which in turn require their own arguments, the dependencies among
%		methods can be explored to identify relevant methods and essential dependency types of MUTs
%		beyond classes under test.
%%	\end{itemize}




\textbf{Composition of method sequences.} FRT generates new method sequences based on concatenation of existing sequences. Existing tools mostly compose sequences by selecting a sequence returning an object with compatible type
at random, but a well designed strategy can optimize this task by considering several factors related to
the cost-effectiveness of testing, such as the time required to execute a sequence
and the likelihood to cover more unexecuted code.
%\end{enumerate}

\subsection{Related Work}

Given the large body of work on automated test case generation,
%~\cite{Marinov:2001:TNF:872023.872551, SPE:SPE602,Pacheco:2007:RFR:1297846.1297902,Jaygarl:2010:OOC:1831708.1831729,ciupa2005automatic,
%Ciupa:2008:AAR:1368088.1368099, Zhang:2011:CSD:2001420.2001463,Fraser:2011:EAT:2025113.2025179},
we discuss work closely related to our approach.
%We also discuss some related work on symbolic execution, concolic testing, model based testing,
%and evolutionary testing. 
For more background,
we refer readers to representative surveys%
~\cite{Anand:2013:OSM:2503903.2503991,McMinn:2004:SST:1077276.1077279,Pasareanu:2009,Fraser:2012:SEE:2337223.2337245}.

%\noindent\textit{\textbf{Variants of Random Testing.}}
\subsubsection{Variants of Random Testing}
The critical step in automatic test case generation for object-oriented programs is to 
prepare the input objects with desirable object states. 
An input object can be constructed
by either direct construction~\cite{Boyapati:2002:KAT:566172.566191,
Marinov:2001:TNF:872023.872551} or
method sequence construction to return the desired 
objects~\cite{Pacheco:2007:RFR:1297846.1297902,Thummalapenta:2009:MOU:1595696.1595725,
Zheng:2010:RUG:1858996.1859054}.
Direct construction approaches, such as Korat~\cite{Boyapati:2002:KAT:566172.566191} 
and TestEra~\cite{Marinov:2001:TNF:872023.872551}, construct objects by
assigning fields directly. These approaches require specifications
defined in languages, such as Alloy, so they are not fully automated.

Most existing random techniques create the required input objects
by \emph{method sequence construction}~\cite{SPE:SPE602,Pacheco:2005:EAG:2144892.2144921,
Pacheco:2007:RFR:1297846.1297902,Thummalapenta:2009:MOU:1595696.1595725, Zhang:2011:CSD:2001420.2001463}.
JCrasher~\cite{SPE:SPE602} creates input objects by using a parameter graph
%(similar to our method dependency graph described in Section~\ref{subsec:detective}) 
to find method type dependencies. Eclat~\cite{Pacheco:2005:EAG:2144892.2144921} 
and Randoop~\cite{Pacheco:2007:RFR:1297846.1297902, Pacheco:2008:FEN:1390630.1390643} use feedback from
previous tests to generate future tests. While sharing the basic idea of FRT, 
GRT substantially improves the entire testing process through a combination of static and dynamic analysis.

\emph{Adaptive random testing}~(ART)~\cite{Chen:2004:ART:2105774.2105800,Anand:2013:OSM:2503903.2503991}
has been proposed to improve the bug detection effectiveness of random testing 
%based on the concept of
by evenly spreading test input selection across the input domain.
Since its introduction by Chen et al.~\cite{Chen:2001:PSS:544998.545004},
various studies~\cite{Chen:2004:ART:2105774.2105800, Ciupa:2008:AAR:1368088.1368099, Lin:2009:DAA:1747491.1747522} have shown that ART requires fewer tests to detect program failures than random testing.
However, ART has also been shown to have high computational overhead~\cite{Arcuri:2011:ART:2001420.2001452,Park:2012:CAH:2393596.2393636} and have difficulties in handling practical larger software that 
requires complex inputs~\cite{Anand:2013:OSM:2503903.2503991}.


%It selects test inputs evenly across the input space.
%ART has been shown to improve the efficiency of random testing and to reduce the number 
%of tests required to reveal the first error.
% ARTOO~\cite{Ciupa:2008:AAR:1368088.1368099} extends 
%the ideas of ART to object-oriented languages by defining object distances based on types and 
%their matching fields. 
%However, ART
%faces the challenge of high-dimensional input spaces.
%which limits the usage of ART to larger software.

%Like most related tools, GRT conforms to the declared
%access modifiers by not calling private
%or protected methods. Previous work reveals that directly
%testing these methods~(by changing non-public modifiers to public)
%increases code coverage~\cite{5254269}.
%However, such tests are difficult to validate.
%By convention,
%a public method should throw an exception if a precondition is violated.%
%\footnote{\url{http://oracle.com/technetwork/java/effective-exceptions-092345.html}}
%Non-public methods may also use assertions, which normally indicate an internal
%flaw in the software, and thus a failed test.%
%\footnote{\url{http://docs.oracle.com/javase/7/docs/technotes/guides/language/assert.html}}


%\noindent\textit{\textbf{Random Testing Guided by Domain Knowledge.}}
\subsubsection{Random Testing Guided by Domain Knowledge}
Existing test cases contain information on valid test sequences.
To explore more valid sequences and
generate more diverse object states,
MSeqGen~\cite{Thummalapenta:2009:MOU:1595696.1595725} mines frequently used
sequence patterns from code bases
to guide sequence generation. RecGen~\cite{Zheng:2010:RUG:1858996.1859054}
performs lightweight analysis on fields accessed by different methods
and favors testing methods that access the same fields together. 
Palus~\cite{Zhang:2011:CSD:2001420.2001463} trains a method sequence model
from existing test cases, and uses such a model to guide test generation at run-time.
%performs dynamic analysis on provided sample test cases to train method sequence models,
%and static analysis to identify method relevance based on field accesses.
%Both results are used to guide run-time test case generation.
OCAT~\cite{Jaygarl:2010:OOC:1831708.1831729} adopts object capture-and-replay techniques, 
where object states are captured from the running sample test cases and
then used as input for further testing.
%Compared to Palus, OCAT does not use sequence to
%save the created objects, which would be difficult to repeat to construct the same object by programmers.
%Robinson et al. modify Randoop to generate oracles for regression tests~\cite{conf/kbse/RobinsonEPAL11}.
Similar to these techniques, GRT also makes use of the domain knowledge to guide random testing. 
However, GRT extracts the domain knowledge from
program under test fully automated without requiring manual effort.

\subsubsection{Systematic Testing}

In contrast to random testing, \emph{symbolic execution} represents input as symbolic values and executes
the program based on abstract semantics, computing path conditions based
on input parameters by leveraging constraint solvers.
Tools like Java PathFinder~\cite{Visser:2004:TIG:1013886.1007526} and
Symbolic PathFinder~\cite{Luckow:2014:SPV:2557833.2560571} generate test cases in this way.
Hybrid approaches of random (concrete) and symbolic execution, called
\emph{concolic} execution, are implemented by
tools like DART~\cite{Godefroid:2005:DDA:1064978.1065036},
Cute and JCute~\cite{Sen:2005:CCU:1095430.1081750, Sen:2006:CJC:2135909.2135962},
Pex~\cite{Tillmann:2008:PWB:1792786.1792798}, and Dsc~\cite{Islam:2010:DTC:1868321.1868326}.

%Our approach uses a light-weight static analysis instead of symbolic techniques to
%approximate path conditions.

An alternative to symbolic execution is \emph{bounded exhaustive testing}~\cite{Marinov:2001:TNF:872023.872551,
Boyapati:2002:KAT:566172.566191,Xie:2005:SFG:2140653.2140685},
which exhaustively generates method sequences up to a small bound of sequence length. 
However, real-world software usually requires longer test sequences to examine more program states beyond
a small bound.


\subsubsection{Evolutionary Testing}
Evolutionary testing~\cite{Tonella:2004:ETC:1007512.1007528,
Baresi:2010:TAU:1810295.1810353,Fraser:2013:WTS:2478542.2478706,Fraser:2011:EAT:2025113.2025179}
leverages the evolutionary algorithm to evolve and search for test sequences that 
optimizes the their fitness~(goodness), e.\,g.,~branch coverage, in a limited search budget. 
%The state of the art evolutionary testing tool,
EvoSuite~\cite{Fraser:2013:WTS:2478542.2478706,Fraser:2011:EAT:2025113.2025179,Galeotti:2014:EST:2610384.2628049} adopts such an approach and goes beyond traditional techniques.
It adopts a hybrid approach to automatically generate and optimize the whole  test suites towards 
satisfying coverage criterion. It has been shown effective in achieving high coverage on practical software~\cite{Fraser:2012:SEE:2337223.2337245}.
%Since Evosuite is the robustness to work on practical software and integrated with many advanced
%testing techniques, 



%EvoSuite~\cite{Fraser:2013:WTS:2478542.2478706}, 
%generate test case fully automatically, 

% trying to generate
%new sequences with diverse object states.
%Compared with these techniques, our approach does not depend on existing
%test cases, which are often biased towards certain types of test 
%sequences~\cite{Calikli:2010:EAF:1868328.1868344}.



%\section{Weaknesses Found in Randoop}
%\label{sec:weaknesses}
%
%
%
%Existing random testing techniques suffer from low structural coverage
%on practical programs,
%where many diverse input object states are required to cover all code.
%Although formal specifications with well-defined input and method invocation constraints enhance random
%testing by shrinking the possible input space, they are
%often unavailable or incomplete in practice. This limits their applicability.
%
%Randoop is fully automated, using no specification or model.
%This makes Randoop very easy
%to use; however, there are still areas in which code coverage is less
%than optimal.
%
%\subsection{Small Initial Value Pool}
%Randoop stores a small set of simple constants for primitive types in
%the initial prefixed value pool,
%such as -1, 0, 1, 10, 100 for bytes, or "hi!" and "" for strings.
%It incrementally grows its initial object pool by storing 
%objects derived from executed test sequences.
%However, the lack of diversity in the beginning
%causes it to miss many branch conditions. 
%Furthermore, Randoop cannot observe primitive temporary values that
%are used inside a method.
%Therefore, even when including all initial values and the 
%values obtained at run-time, the small value pool still
%limits the achievable coverage.
%Consider the example from class \texttt{PatternOptionBuilder} in 
%Fig.~\ref{fig:constant-m}: Branches of method \texttt{getValueClass}
%are not covered by Randoop as it does not contain predefined primitive values,
%or values derived from them, to satisfy these branch conditions at run-time.
%However, using primitive values such as '@' as an input would easily cover a branch in this case.
%
%%Figure~\ref{fig:constant-m} shows a code example of such a case.
%%the following could be removed if no more space is allowed.
%
%\subsection{No Distinction on Side Effects}
%Only methods with side effects can change the state of an object~\cite{Salcianu:2005:PSE:2131753.2131773}.
%These methods should 
%be favored over methods without side effects in order to frequently mutate 
%object states and, thus, to satisfy more branch conditions.
%Since Randoop does not distinguish between methods with and without side effects,
%it creates long and redundant sequences for objects staying in the same
%state. All of these sequences are put into the pool of reusable sequences,
%deferring state changes even further and slowing down coverage growth.%
%%%% footnote no longer fits here
%%\footnote{Object equality can be checked based on their identity or
%%on their contents (if a corresponding \texttt{equals} method is provided).
%%Randoop identifies only whether two sequences equivalent, but not whether their
%%execution results match.}
%
%\subsection{Static Type Management}
%Randoop uses the static (declared) return type of a method to manage
%a successful sequence in the object pool.
%Static type management does not distinguish whether 
%a method produces the declared return type or a subtype at run-time.
%It therefore may not cover all possible behaviors of a method,
%causing some methods never to be tested because no compatible input
%seems available, even if a sequence that returns the required type at run-time
%does exist.
%This limitation may result from the need to easily generate compilable
%code for off-line testing, which requires the correct type casts to be
%added if the dynamic type of a variable does not correspond to its static type.
%
%In our example in Fig.~\ref{fig:constant-m}, branch coverage in method
%\texttt{createVal} requires both
%suitable primitive values and a class descriptor returned by
%\texttt{getValueClass} method. However, static type management stores
%the object returned by \texttt{getValueClass} as type \texttt{Object}
%according to its declaration. This results in \texttt{createVal} never
%being directly tested.
%
%\subsection{Fixed Method Pool}
%Randoop uses a fixed method pool when considering methods to test.
%However, many methods require an input type that cannot
%be generated from invoking a fixed set of API methods;
%instead, they may require data from libraries not belonging to the software
%under test itself.
%When a method requires an input type that cannot be generated
%from existing input sequences, Randoop simply skips that method.
%Because of this, many MUTs are never tested.%
%%\footnote{Some of these methods
%%may still be indirectly invoked from other methods that are directly tested.}
%
%Consider class \texttt{IOUtils} (see Fig.~\ref{fig:compress-m}), where
%both methods \texttt{copy} and \texttt{skip}
%require an object of type \texttt{InputStream}. However, the required
%object is never
%generated by Randoop as it requires using the Java core library.
%Due to this, no method of this class is ever covered,
%even though providing the required input easily covers most of these methods.
%
%\subsection{Lengthy Input Sequences}
%Randoop manages all generated successful sequences in its object pool 
%such that sequences that return the same type are put into the same set. 
%Whenever an input with a specific type is required, Randoop randomly selects
%a sequence among all available sequences.
%Since new sequences are constructed by concatenating existing input
%sequences, the resulting sequences can grow considerably in length.
%This adds to the execution cost of generated test cases.
%Randoop quickly reaches a bottleneck after running for several minutes,
%repeatedly executing lengthy sequences while leaving many other
%relevant sequence combinations untested.
%
%
%\subsection{No Coverage-Based Guidance}
%The difficulty of covering a branch varies between branches. 
%Some branches are easily covered in the early phase of random
%testing, while others require a complex object state. An equally balanced 
%selection of methods to be tested, wastes time on those methods
%that are already well covered.
%On the other hand, too much emphasis on uncovered branches may
%waste time in challenging the difficult branches without much payoff.
%Our observation is that the current strategy of Randoop is not ideal,
%but the solution is not as simple as looking for uncovered 
%branches~\cite{Jaygarl:2010:GTG}.
