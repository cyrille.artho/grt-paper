#!/bin/sh
# convert LyX bug table
LYX=lyx
[ -e ${LYX} ] || LYX=/Applications/LyX.app/Contents/MacOS/lyx
${LYX} -e latex bug-eval.lyx
sed -n '/^\\begin{tabular}/,/^\\end{tabular}/p' bug-eval.tex > bug-table.tex
