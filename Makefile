#!/usr/bin/make -f
#
# $Id: Makefile,v 1.4 2007/04/04 04:45:45 cartho Exp $
#

.PHONY: all ps pdf clean pics runclean

# --- programs
TEX=latex
PDFTEX=pdflatex
DVIPS=dvips

# --- files

FILENAME=RandomTesting

# ---------------------------------------------------------------------

all:	pdf

%ps:	$(FILENAME).ps pics

pdf:	$(FILENAME).pdf pics

# --- generic rules

%.eps: %.fig
	$(FIG2DEV) -L eps <$*.fig >$@

%.dvi: %.tex biblio.bib bug-table.tex pics benchmarkDescription-table.tex 
	-$(TEX) $*.tex
	bibtex $*
	$(TEX) $*.tex
	$(TEX) $*.tex
#	$(TEX) $*.tex

%.ps:	$(FILENAME).dvi pics
	$(DVIPS) -f $*.dvi >$@	

%.pdf:	$(FILENAME).tex biblio.bib bug-table.tex pics benchmarkDescription-table.tex 
	-$(PDFTEX) $*.tex
	bibtex $* 
	$(PDFTEX) $*.tex
	$(PDFTEX) $*.tex
#	$(PDFTEX) $*.tex

# --- specific rules

pics:
	make -C workflow

tar: pics ${FILENAME}.dvi
	cd ..; \
	tar --exclude .svn\* \
	-cvzf ${FILENAME}-leima.tar.gz ${FILENAME}/*.{tex,bib,dvi} \
	${FILENAME}/{Makefile,pics}; \
	cd ${FILENAME}

runclean:
	rm -f $(FILENAME).log $(FILENAME).aux $(FILENAME).toc *.blg *.bbl *.out

# add figclean below once there are any figures
clean:	runclean
	rm -f $(FILENAME).dvi $(FILENAME).ps  $(FILENAME).pdf
	make -C workflow clean

distclean: clean
