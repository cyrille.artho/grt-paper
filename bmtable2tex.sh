#!/bin/sh
# convert LyX bug table
LYX=lyx
[ -e ${LYX} ] || LYX=/Applications/LyX.app/Contents/MacOS/lyx
${LYX} -e latex benchmarks_ICSE.lyx
sed -n '/^\\begin{tabular}/,/^\\end{tabular}/p' \
benchmarks_ICSE.tex > benchmarkDescription-table.tex
