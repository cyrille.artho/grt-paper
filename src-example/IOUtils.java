package org.apache.commons.compress.utils;
public final class IOUtils {
  private IOUtils() { }
  public static long copy(final InputStream input,
    final OutputStream output) throws IOException {
    return copy(input, output, 8024); }
  public static long skip(InputStream input, long n)
    long available = n;
    while (n > 0) {
      long skipped = input.skip(n);
      if (skipped == 0) break;
      n -= skipped;
    }
    return available - n; } } // 5 methods omitted.
