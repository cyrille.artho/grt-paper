package org.apache.commons.cli;
public class PatternOptionBuilder{
  public static final Class STRING_VALUE = String.class;
  public static final Class OBJECT_VALUE = Object.class;
  // 7 more similar fields omitted.
  public static Object getValueClass(char ch) {
    switch (ch) {
     case '@': return PatternOptionBuilder.OBJECT_VALUE;
     case ':': return PatternOptionBuilder.STRING_VALUE;
     // 7 more case branches ommited.
    }
    return null;
  } // 2 more methods omitted.
}
public class TypeHandler {
  //commnent this method in current version
//  public static Object createValue(String str, Object o)
//  { return createValue(str, (Class) o); }
    
  public static Object createValue(String str, Class c) {
    if (PatternOptionBuilder.STRING_VALUE == c)
      return str; 
    else if (PatternOptionBuilder.OBJECT_VALUE == c)
      return createObject(str);
    // 7 more else if branches omitted.
    else return null; } // 7 more methods omitted.
} 
