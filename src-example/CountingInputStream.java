package org.apache.commons.compress.utils;
public class CountingInputStream 
              extends FilterInputStream {
  private long bytesRead;
  public CountingInputStream(final InputStream in) 
  {super(in);}
  \*Override*\
  public int read() throws IOException {
    int r = in.read();
    if (r >= 0) {
    count(1);
    }
    return r;
  }
    ... 4 more methods
}