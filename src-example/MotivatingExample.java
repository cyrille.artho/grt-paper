package org.apache.commons.cli;
public class PatternOptionBuilder{
  public static final Class STRING_VAL=String.class;
  public static final Class OBJECT_VAL=Object.class;
  // 7 more similar fields omitted.
  public static Object getValueClass(char ch) {
    switch (ch) {
     case '@':return PatternOptionBuilder.OBJECT_VAL;
     case ':':return PatternOptionBuilder.STRING_VAL;
     // 7 more case branches omitted.
    }
    return null;
} } // 2 more methods omitted.
public class TypeHandler {
  // 1 method omitted.
  public static Object createVal(String s, Class c) {
    if (PatternOptionBuilder.STRING_VAL == c)
      return s; 
    else if (PatternOptionBuilder.OBJECT_VAL == c)
      return createObject(s);
    // 7 more else if branches omitted.
    else return null; } } // 7 more methods omitted.
