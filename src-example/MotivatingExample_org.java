package org.apache.commons.codec.language;
public class MatchRatingApproachEncoder 
                 implements StringEncoder {
    private static final String SPACE = " ";
    private static final String EMPTY = "";
    private static final int ONE = 1, TWO = 2, 
            THREE = 3, FOUR = 4, FIVE = 5,...;
... 4 methods omitted here...
    int getMinRating(final int sumLength) {
        int minRating = 0;
        if (sumLength <= FOUR) {
            minRating = FIVE;
        } else if ...
    }
    public boolean isEncodeEquals(String name1, 
                                  String name2) {
        if (name1 == null || 
          EMPTY.equalsIgnoreCase(name1)
         || SPACE.equalsIgnoreCase(name1)) {...
    }
... 5 methods omitted here...
}
