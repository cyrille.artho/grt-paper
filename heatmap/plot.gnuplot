# generate heat map

set terminal postscript eps enhanced color "Helvetica" 18

set title "Relative change in statement coverage"

set out "scch_heatmap.ps"

set palette defined (-0.5 "red", 0 "white", 1 "green")

set cbrange [-0.5:1]
set cblabel "Relative change"

set xrange [-0.5:6.5]
set yrange [-0.5:25.5]

set xtics ("Constant Mining" 0.0, "\nImpurity" 1.0, "Elephant Brain" 2.0, "\nDetective" 3.0, "Orienteering" 4.0, "\nBloodhound" 5.0, "Combined" 6.0)

set view map
splot 'scch.data' matrix with image
